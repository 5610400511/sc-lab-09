package Model;

import Interface.Measurable;
import Interface.Taxable;

public class Person implements Measurable, Taxable , Comparable {

	private String name;
	private double height;
	private double yearlyIncome;


	public Person(String name, double height) {
		this.name = name;
		this.height = height;
	}

	public Person(String name, double height, double yearlyIncome) {
		this(name, height);
		this.yearlyIncome = yearlyIncome;
	}

	public String getName() {
		return this.name;
	}

	public double getHeight() {
		return this.height;
	}

	public double getYearlyIncome() {
		return this.yearlyIncome;
	}

	@Override
	public double getMeasure() {
		return this.getHeight();
	}

	@Override
	public double getTax() {
		double income = this.getYearlyIncome();
		double result = 0;

		if (income > 300000) {
			result += 0.05 * 300000;
			income -= 300000;
			result += 0.10 * income;
		}else{
			result += 0.05 * income;
		}
		return result;
	}

	@Override
	public int compareTo(Object obj) {
		  if (this.yearlyIncome == ((Person) obj).getYearlyIncome())
	            return 0;
	        else if ((this.yearlyIncome) > ((Person) obj).getYearlyIncome())
	            return 1;
	        else{
	        	return -1;
	        }
	}
	public String toString() {
        return "Name " + name + " Income " + yearlyIncome;
    }
	

}
