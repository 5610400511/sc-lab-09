package Model;

import Interface.Taxable;

public class Product implements Taxable, Comparable {
	private String name;
	private double price;

	public Product(String name, double price) {
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return this.name;
	}

	public double getPrice() {
		return this.price;
	}

	@Override
	public double getTax() {
		return 0.07 * this.getPrice();
	}

	@Override
	public int compareTo(Object obj) {
		if (this.price == ((Product) obj).getPrice())
			return 0;
		else if ((this.price) > ((Product) obj).getPrice())
			return 1;
		else {
			return -1;
		}
	}
	public String toString() {
        return  name + " Price " + price;
    }
}
