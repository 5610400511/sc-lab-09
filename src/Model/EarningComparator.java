package Model;

import java.util.Comparator;

public class EarningComparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		Company s1=(Company)o1;  
		Company s2=(Company)o2;  
		  
		if (s1.getIncome() == s2.getIncome())
			return 0;
        else if ((s1.getIncome()) > s2.getIncome())
            return 1;
        else{
        	return -1;
        }
	}

}
