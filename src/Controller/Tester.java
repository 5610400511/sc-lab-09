package Controller;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import Interface.Measurable;
import Interface.Taxable;
import Model.Company;
import Model.Country;
import Model.Data;
import Model.EarningComparator;
import Model.ExpenseComparator;
import Model.Person;
import Model.Product;
import Model.ProfitComparator;
import Model.TaxCalculator;

public class Tester {

	public static void main(String[] args) {
		Tester tester = new Tester();
		tester.testPerson();
		tester.testProduct();
		tester.testCompany();
		tester.testTax();
	}

	private void testTax() {
		ArrayList<Object> taxobj = new ArrayList <Object>();
		taxobj.add(new Person("John", 185, 158900));
		taxobj.add(new Product("Mazda3 Skyactive", 380000));
		taxobj.add(new Company("CP Company", 1800000, 500000));
		
	}

	public void testPerson() {

		ArrayList<Person> personlist = new ArrayList<Person>();
		personlist.add(new Person("Kog", 145, 120406));
		personlist.add(new Person("Pla", 180, 165000));
		personlist.add(new Person("Earl", 175, 1200));
		Collections.sort(personlist);

		Iterator itr = personlist.iterator();

		while (itr.hasNext()) {
			Object element = itr.next();
			System.out.println(element + "\n");

		}
		System.out.println("##############################################");
	}

	public void testProduct() {
		ArrayList<Product> productlist = new ArrayList<Product>();
		productlist.add(new Product("Icitan", 18));
		productlist.add(new Product("Mazda2", 380000));
		productlist.add(new Product("Nike shoe", 1750));
		Collections.sort(productlist);

		Iterator itr = productlist.iterator();

		while (itr.hasNext()) {
			Object element = itr.next();
			System.out.println(element + "\n");

		}
		System.out.println("##############################################"
				+ "\n");
	}

	public void testCompany() {
		ArrayList<Company> CPNlist = new ArrayList<Company>();
		CPNlist.add(new Company("CP Company", 1800000, 500000));
		CPNlist.add(new Company("Honda group", 60000, 300000));
		CPNlist.add(new Company("Boonrod", 1750000, 1700000));

		Collections.sort(CPNlist, new EarningComparator());
		Iterator itr = CPNlist.iterator();
		System.out.println("##########Sort By Income##########");
		while (itr.hasNext()) {
			Object element = itr.next();
			System.out.println(element + "\n");

		}
		Collections.sort(CPNlist, new ExpenseComparator());
		Iterator itr2 = CPNlist.iterator();
		System.out.println("##########Sort By Expense##########");
		while (itr2.hasNext()) {
			Object element = itr2.next();
			System.out.println(element + "\n");
		}
		
		Collections.sort(CPNlist, new ProfitComparator());
		Iterator itr3 = CPNlist.iterator();
		System.out.println("##########Sort By Profit##########");
		while (itr3.hasNext()) {
			Object element = itr3.next();
			System.out.println(element + "\n");
		}
	}
}
