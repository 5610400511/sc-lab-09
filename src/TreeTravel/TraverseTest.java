package TreeTravel;

public class TraverseTest {
	public static void main(String[] args){
		Node h = new Node("H",null,null);
		Node i = new Node("I",h,null);
		Node g = new Node("G",null,i);
		Node e = new Node("E",null,null);
		Node c = new Node("C",null,null);
		Node d = new Node("D",c,e);
		Node a = new Node("A",null,null);
		Node b = new Node("B",a,d);
		Node f = new Node("F",b,g);
		
		PreOrderTraversal preorder = new PreOrderTraversal();
		InOrderTraversal inorder = new InOrderTraversal();
		PostOrderTraversal postorder = new PostOrderTraversal();
		
		ReportConsole con = new ReportConsole();
		
		con.display(f, preorder);
		con.display(f, inorder);
		con.display(f, postorder);
		
	}
}
