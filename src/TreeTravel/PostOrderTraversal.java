package TreeTravel;
import java.util.ArrayList;
import java.util.Stack;


public class PostOrderTraversal implements Traversal{

	@Override
	public ArrayList<Node> traverse(Node node) {
		ArrayList<Node> ans = new ArrayList<Node>();
		Stack<Node> stack = new Stack<Node>();
		Node current = node;
		stack.push(node);
		Node prev = null;
		
		while (!stack.isEmpty()){
			current = stack.peek();
			if(prev == null || prev.getLeft() == current || prev.getRight() == current){
				if(current.getLeft() != null){
					stack.push(current.getLeft());
				}
				else if(current.getRight() != null){
					stack.push(current.getRight());
				}
				else{
					ans.add(current);
					stack.pop();
				}
			}
			
			if(current.getLeft() == prev){
				if(current.getRight()!=null){
					stack.push(current.getRight());
				}
				else{
					ans.add(current);
					stack.pop();
				}
			}
			
			if(current.getRight()== prev){
				ans.add(current);
				stack.pop();
			}
			prev = current;
		}
		return ans;
	}

	public String toString(){
		return "PostOrderTraversal";
	}
}
