package TreeTravel;
import java.util.ArrayList;


public interface Traversal {
	public ArrayList<Node> traverse(Node node);
}
