package TreeTravel;
import java.util.ArrayList;
import java.util.Stack;


public class PreOrderTraversal implements Traversal{

	@Override
	public ArrayList<Node> traverse(Node node) {
		ArrayList<Node> ans = new ArrayList<Node>();
		Stack<Node> stack = new Stack<Node>();
		
		stack.push(node);
		
		while (!stack.isEmpty()){
			Node var = stack.pop();
			ans.add(var);
			
			if (var.getRight() != null){
				stack.push(var.getRight());
			}
			
			if (var.getLeft() != null){
				stack.push(var.getLeft());
			}
		}
		
		return ans;
}
	public String toString(){
		return "PreOrderTraversal";
	}
}
